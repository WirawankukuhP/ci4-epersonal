--------------- Cara Install --------------
1. Pastikan mempunyai database server seperti XAMPP
2. Pastikan PHP yang dimiliki versi 8.1 ke atas
3. Setelah mendownload / meng-clone data dari GitHub, ubah env_example menjadi .env 
	& sesuaikan dengan konfigurasi Localhost masing masing PC
4. Setelah itu, dapat memasukkan database baik dari migrate ataupun secara manual menggunakan file .sql
5. Jika menggunakan migrate, jangan lupa eksekusi seedernya : php spark db:seed DataSeeder (berisikan data data dari 2 table)
6. Selamat menggunakan Aplikasi E-Personal
