<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
//Route index home bawaan CI4
$routes->get('/', 'Home::index');
$routes->get('/dashboard', 'dashboard::index');
//Route login, auth untuk melakukan validasi username dan password
$routes->get('login', 'AuthController::login');
$routes->match(['get', 'post'], 'login/auth', 'AuthController::loginAuth');
$routes->get('logout', 'AuthController::logout');
// $routes->get('dashboard', 'DashboardController::index', ['filter' => 'auth']);

//Route pegawai untuk halaman table
$routes->get('pegawai/index', 'Pegawai::index');
$routes->get('pegawai/index/(:num)', 'Pegawai::index/$1');
// ------------
//Route pegawai index, untuk view data pegawai
$routes->get('pegawai', 'Pegawai::index', ['filter' => 'auth']);
//Route pegawai get create, untuk menampilkan tambah data pegawai
$routes->get('pegawai/create', 'Pegawai::create', ['filter' => 'auth']);
//Route pegawai post store, untuk proses tambah data pegawai
$routes->post('pegawai/store', 'Pegawai::store');
//Route pegawai get edit, untuk proses ambil data pegawai dari table pegawai
$routes->get('pegawai/edit/(:num)', 'Pegawai::edit/$1', ['filter' => 'auth']);
//Route pegawai post edit, untuk proses update data pegawai ke table pegawai
$routes->post('pegawai/update/(:num)', 'Pegawai::update/$1');
//Route pegawai get delete, untuk proses hapus data pegawai dari table pegawai
$routes->get('pegawai/delete/(:num)', 'Pegawai::delete/$1');

//Route pegawai untuk halaman table
$routes->get('user/index', 'User::index');
$routes->get('user/index/(:num)', 'User::index/$1');
// ------------
//Route user index, untuk view data user
$routes->get('user', 'User::index', ['filter' => 'auth']);
//Route user get create, untuk menampilkan tambah data user
$routes->get('user/create', 'User::create', ['filter' => 'auth']);
//Route user post store, untuk proses tambah data user
$routes->post('user/store', 'User::store');
//Route user get edit, untuk proses ambil data user dari table users
$routes->get('user/edit/(:num)', 'User::edit/$1', ['filter' => 'auth']);
//Route user post edit, untuk proses update data user ke table users
$routes->post('user/update/(:num)', 'User::update/$1');
//Route user get delete, untuk proses hapus data user dari table users
$routes->get('user/delete/(:num)', 'User::delete/$1');
?>
