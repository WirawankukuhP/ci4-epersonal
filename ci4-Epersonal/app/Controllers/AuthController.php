<?php namespace App\Controllers;

use App\Models\UserModel;

class AuthController extends BaseController
{
    public function login()
    {
        helper(['form']);
        echo view('login');
    }

    public function loginAuth()
    {
        $session = session();
        $model = new UserModel();
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $data = $model->where('username', $username)->first();

        if($data){
            $pass = $data->password;
            $authenticatePassword = password_verify($password, $pass);
            if($authenticatePassword){
                $ses_data = [
                    'id_user'       => $data->id_user, 
                    'username' => $data->username,
                    'isLoggedIn' => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('dashboard');
            }else{
                $session->setFlashdata('msg', 'Password is incorrect.');
                return redirect()->to('login');
            }
        }else{
            $session->setFlashdata('msg', 'Username does not exist.');
            return redirect()->to('login');
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('login');
    }
}