<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
    public function index(): string
    {
    $pegawaiModel = new \App\Models\PegawaiModel();
    $userModel = new \App\Models\UserModel();

    $data['jumlahPegawai'] = $pegawaiModel->getCount();
    $data['jumlahUser'] = $userModel->getCount();

    return view('dashboard', $data);
    }
}
