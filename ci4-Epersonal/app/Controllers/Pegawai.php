<?php
namespace App\Models;
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\PegawaiModel;

class Pegawai extends BaseController
{
    protected $pegawai;
    function __construct()
    {
        $this->pegawai = new PegawaiModel();
    }
    public function index($page = 1) : string
    {
        $model = new \App\Models\PegawaiModel();
        $dataPerPage = 4;
        $totalData = $model->countAllResults();
        $totalPages = ceil($totalData / $dataPerPage);

        $data['pegawai'] = $model->orderBy('id_pegawai', 'ASC')
                              ->findAll($dataPerPage, ($page - 1) * $dataPerPage);
        $data['currentPage'] = $page;
        $data['totalPages'] = $totalPages;
        // $data['pegawai'] = $this->pegawai->findAll();
        return view('pegawai/index', $data); 
   }
   // create, menuju view create data pegawai
   public function create()
    {
        return view('pegawai/create');
    }
    // store, untuk proses tambah data pegawai
    public function store()
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi',
                ]
            ],
            'jenis_kelamin' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'no_telp' => [
                'rules' => 'required|is_unique[pegawai.no_telp]|max_length[13]',
                'errors' => [
                    'required' => '{field} Harus diisi',
                    'is_unique' => 'Nomor telepon sudah terdaftar.',
                    'max_length' => 'Nomor telepon tidak boleh lebih dari 13'
                ]
            ],
            'email' => [
                'rules' => 'required|valid_email|is_unique[pegawai.email]',
                'errors' => [
                    'required' => '{field} Harus diisi',
                    'is_unique' => 'Email sudah terdaftar.',
                    'valid_email' => 'Email Harus Valid'
                ]
            ],
            'alamat' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
 
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }
 
        $this->pegawai->insert([
            'nama' => $this->request->getVar('nama'),
            'jenis_kelamin' => $this->request->getVar('jenis_kelamin'),
            'no_telp' => $this->request->getVar('no_telp'),
            'email' => $this->request->getVar('email'),
            'alamat' => $this->request->getVar('alamat')
        ]);
        session()->setFlashdata('message', 'Tambah Data Pegawai Berhasil');
        return redirect()->to('pegawai');
    }
    // edit, untuk proses ambil data pegawai dari table pegawai
    function edit($id)
    {
        $dataPegawai = $this->pegawai->find($id);
        if (empty($dataPegawai)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Pegawai yang dipilih Tidak ditemukan !');
        }
        $data['pegawai'] = $dataPegawai;
        return view('pegawai/edit', $data);
    }
    // update, untuk proses update data pegawai ke table pegawai
    public function update($id)
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'jenis_kelamin' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'no_telp' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => '{field} Harus diisi',
                    'valid_email' => 'Email Harus Valid'
                ]
            ],
            'alamat' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
 
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back();
        }
 
        $this->pegawai->update($id, [
            'nama' => $this->request->getVar('nama'),
            'jenis_kelamin' => $this->request->getVar('jenis_kelamin'),
            'no_telp' => $this->request->getVar('no_telp'),
            'email' => $this->request->getVar('email'),
            'alamat' => $this->request->getVar('alamat')
        ]);
        session()->setFlashdata('message', 'Update Data Pegawai Berhasil');
        return redirect()->to('pegawai');
    }
    // delete, untuk proses delete data pegawai di table pegawai
    function delete($id)
    {
        $dataPegawai = $this->pegawai->find($id);
        if (empty($dataPegawai)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Pegawai yang dipilih Tidak ditemukan !');
        }
        $this->pegawai->delete($id);
        session()->setFlashdata('message', 'Delete Data Pegawai Berhasil');
        return redirect()->to('pegawai');
    }
}
