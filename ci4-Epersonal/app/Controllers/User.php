<?php
namespace App\Models;
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\UserModel;

class User extends BaseController
{
    protected $user;
    function __construct()
    {
        $this->user = new UserModel();
    }
    public function index($page = 1) : string
    {
        $model = new \App\Models\UserModel();
        $dataPerPage = 3;
        $totalData = $model->countAllResults();
        $totalPages = ceil($totalData / $dataPerPage);

        $data['user'] = $model->orderBy('id_user', 'ASC')
                              ->findAll($dataPerPage, ($page - 1) * $dataPerPage);
        $data['currentPage'] = $page;
        $data['totalPages'] = $totalPages;
        // $data['user'] = $this->user->findAll();
        return view('user/index', $data); 
   }
   // create, menuju view create data user
   public function create()
    {
        return view('user/create');
    }
    // store, untuk proses tambah data user
    public function store()
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'email' => [
                'rules' => 'required|valid_email|is_unique[users.email]',
                'errors' => [
                    'required' => '{field} Harus diisi',
                    'is_unique' => 'Email sudah terdaftar.',
                    'valid_email' => 'Email Harus Valid'
                ]
            ],
            'username' => [
                'rules' => 'required|is_unique[users.username]',
                'errors' => [
                    'is_unique' => 'Username sudah terdaftar.',
                    'required' => '{field} Harus diisi'
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'photo' => [
                'rules'         => 'uploaded[photo]|is_image[photo]|mime_in[photo,image/jpg,image/jpeg]|max_size[photo,300]',
                'errors' => [
                    'required' => '{field} Harus diisi',
                    'uploaded' => 'Tidak ada file yang diupload',
                    'is_image' => 'File yang dipilih bukan gambar',
                    'mime_in' => 'Format file tidak didukung',
                    'max_size' => 'Ukuran file terlalu besar. Maksimum adalah 300KB',
                ],
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }
        //ambil terlebih dulu gambarnya
        $filePhoto = $this->request->getFile('photo');
        //lalu pindahkan file kedalam folder images
        $filePhoto->move('images');
        //ambil nama file cover
        $namaPhoto = $filePhoto->getName();

        // Hash password sebelum menyimpan
        $hashedPassword = password_hash($this->request->getVar('password'), PASSWORD_DEFAULT);
 
        $this->user->insert([
            'nama' => $this->request->getVar('nama'),
            'email' => $this->request->getVar('email'),
            'username' => $this->request->getVar('username'),
            'password' => $hashedPassword,
            'photo' => $namaPhoto
        ]);
        session()->setFlashdata('message', 'Tambah Data User Berhasil');
        return redirect()->to('user');
    }

    
    // edit, untuk proses ambil data user dari table user
    function edit($id)
    {
        $dataUser = $this->user->find($id);
        if (empty($dataUser)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data User yang dipilih Tidak ditemukan !');
        }
        $data['user'] = $dataUser;
        return view('user/edit', $data);
    }
    // update, untuk proses update data user ke table user
    public function update($id)
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => '{field} Harus diisi',
                    'valid_email' => 'Email Harus Valid'
                ]
            ],
            'username' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi'
                ]
            ],
            'photo' => [
                'rules'         => 'is_image[photo]|mime_in[photo,image/jpg,image/jpeg]|max_size[photo,300]',
                'errors' => [
                    'is_image' => 'File yang dipilih bukan gambar',
                    'mime_in' => 'Format file tidak didukung',
                    'max_size' => 'Ukuran file terlalu besar. Maksimum adalah 300KB',
                ],
            ],
 
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }
        // Hash password sebelum menyimpan
        $hashedPassword = password_hash($this->request->getVar('password'), PASSWORD_DEFAULT);
        //ambil terlebih dulu gambarnya
        $filePhoto = $this->request->getFile('photo');
        $userData = $this->user->find($id); // Ambil data user sebagai objek
        // //lalu pindahkan file kedalam folder images
        // $filePhoto->move('images');
        // //ambil nama file cover
        // $namaPhoto = $filePhoto->getName();
        if ($filePhoto && $filePhoto->isValid() && !$filePhoto->hasMoved()) {
            // Jika ya, pindahkan file ke dalam folder images dan ambil nama file
            $filePhoto->move('images');
            $namaPhoto = $filePhoto->getName();
        } else {
            // Jika tidak, ambil nama foto yang sudah ada dari database
            $namaPhoto = $userData->photo;
        }

        $this->user->update($id, [
            'nama' => $this->request->getVar('nama'),
            'email' => $this->request->getVar('email'),
            'username' => $this->request->getVar('username'),
            'password' => $hashedPassword,
            'photo' => $namaPhoto
        ]);
        
        session()->setFlashdata('message', 'Update Data User Berhasil');
        return redirect()->to('user');
    }
    // delete, untuk proses delete data user di table user
    function delete($id)
    {
        $dataUser = $this->user->find($id);
        if (empty($dataUser)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data User yang dipilih Tidak ditemukan !');
        }
        $this->user->delete($id);
        session()->setFlashdata('message', 'Delete Data User Berhasil');
        return redirect()->to('user');
    }
}
