<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pegawai extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_pegawai' => [
                'type'   => 'INT',
                'constraint'   => 11,
                'unsigned'   => true,
                'auto_increment'   => true,
            ],
            'nama' => [
                'type'   => 'VARCHAR',
                'constraint'   => '255',
            ],
            'jenis_kelamin' => [
                'type'   => 'ENUM',
                'constraint'   => "'pria','wanita'",
            ],
            'no_telp' => [
                'type'   => 'VARCHAR',
                'constraint'   => '50',
            ],
            'email' => [
                'type'   => 'VARCHAR',
                'constraint'   => '50',
            ],
            'alamat' => [
                'type'   => 'VARCHAR',
                'constraint'   => '255',
            ],
            'created_at' => [
                'type'   => 'DATETIME',
                'constraint'   => true,
            ],
            'updated_at' => [
                'type'   => 'DATETIME',
                'constraint'   => true,
            ]           
        ]);
        $this->forge->addKey('id_pegawai', true);
        $this->forge->createTable('pegawai');
    }

    public function down()
    {
        $this->forge->dropTable('pegawai');
    }
}



