<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_user' => [
                'type'   => 'INT',
                'constraint'   => 11,
                'unsigned'   => true,
                'auto_increment'   => true,
            ],
            'nama' => [
                'type'   => 'VARCHAR',
                'constraint'   => '255',
            ],
            'email' => [
                'type'   => 'VARCHAR',
                'constraint'   => '50',
            ],
            'username' => [
                'type'   => 'VARCHAR',
                'constraint'   => '50',
            ],
            'password' => [
                'type'   => 'VARCHAR',
                'constraint'   => '255',
            ],
            'photo' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'created_at' => [
                'type'   => 'DATETIME',
                'constraint'   => true,
            ],
            'updated_at' => [
                'type'   => 'DATETIME',
                'constraint'   => true,
            ]           
        ]);
        $this->forge->addKey('id_user', true);
        $this->forge->createTable('users');
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}