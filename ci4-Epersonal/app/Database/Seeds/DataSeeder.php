<?php 
namespace App\Database\Seeds;
use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class DataSeeder extends Seeder
{
    public function run()
    {
        $this->call('PegawaiSeeder');
        $this->call('UsersSeeder');
    }
} 