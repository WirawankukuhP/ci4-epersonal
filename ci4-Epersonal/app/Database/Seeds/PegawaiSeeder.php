<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;
class PegawaiSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
            'nama'      => 'Syifa',
            'jenis_kelamin'      => 'wanita',
            'no_telp'      => '08129382930',
            'alamat'      => 'Jl. Sudaya No 18',
            'email'      => 'syifa98@gmail.com',
            'created_at'      => Time::now(),
            ],
            [
            'nama'      => 'Komar',
            'jenis_kelamin'      => 'pria',
            'no_telp'      => '08762638293',
            'alamat'      => 'Jl. Ampera No 220',
            'email'      => 'komar76@gmail.com',
            'created_at'      => Time::now(),
            ],
            [
            'nama'      => 'Jaka',
            'jenis_kelamin'      => 'pria',
            'no_telp'      => '08645267488',
            'alamat'      => 'Jl. Lokapala No 87',
            'email'      => 'jakaminang@gmail.com',
            'created_at'      => Time::now(),
            ]
        ];
        $this->db->table('pegawai')->insertBatch($data);
    }
}


