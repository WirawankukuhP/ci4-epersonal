<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;
class UsersSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
            'nama'      => 'Administrator',
            'email'      => 'admin@gmail.com',
            'username'      => 'admin',
            'password'      => password_hash('admin123', PASSWORD_DEFAULT),
            'photo'      => 'admin.jpg',
            'created_at'      => Time::now(),
            ],
            [
            'nama'      => 'User',
            'email'      => 'user@gmail.com',
            'username'      => 'user',
            'password'      => password_hash('user123', PASSWORD_DEFAULT),
            'photo'      => 'user.jpg',
            'created_at'      => Time::now(),
            ]
            
        ];
        $this->db->table('users')->insertBatch($data);
    }
}
