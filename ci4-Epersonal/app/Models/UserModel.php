<?php
namespace App\Models;
use CodeIgniter\Model;

class UserModel extends Model
{
protected $table = "users";
protected $primaryKey = "id_user";
protected $returnType = "object";
protected $useTimestamps = true;
protected $allowedFields = ['id_user','nama','email','username','password','photo'];

public function getCount() {
    return $this->countAll();
}
}
?>