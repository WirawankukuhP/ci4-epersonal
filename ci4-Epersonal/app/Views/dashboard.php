<!doctype html>
<html lang="en" class="h-100">
 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Page | Dashboard</title>
 
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sticky-footer-navbar/">
 
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
 
    <style>
        /* Custom CSS untuk tampilan card yang lebih elegan */
.card-custom {
    border-radius: 15px;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
}

.card-custom:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.card-header-custom {
    background-color: #4e73df;
    color: white;
    border-bottom: none;
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
}

.card-body-custom {
    background-color: #f8f9fc;
}

.card-title-custom {
    color: #white;
    font-weight: bold;
}

.btn-custom {
    background-color: #4e73df;
    color: white;
    border: none;
}

.btn-custom:hover {
    background-color: #2e59d9;
    color: white;
}

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
 
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.5/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">
</head>
 
<body class="d-flex flex-column h-100">
    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">E-Personal</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                        <a class="nav-link active" href="<?= base_url('home') ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('pegawai') ?>"> Manajemen Pegawai </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('user') ?>"> Manajemen User </a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="logout" onclick="return confirm('Apakah Anda yakin ingin Keluar dari Aplikasi E-Personal?')">Logout </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
 
    <!-- Begin page content -->
    <main role="main" class="flex-shrink-0">

    <!-- Dashboard Content -->
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
        <h1 class="text-center">
           Selamat Datang di E-Personal 
           <h3 class="text-center">"Layanan Manajemen Kepegawaian Perusahaan Yang User Friendly"</h3>
           <p class="text-center">Anda dapat mengetahui banyaknya data Pegawai dan User di Aplikasi ini</p>
        </h1>
            <div class="card-deck mt-7 text-center">
                <!-- Card Example -->
                <div class="card card-custom">
                    <div class="card-header card-header-custom">
                        <h5 class="card-title card-title-custom">Manage Pegawai</h5>
                    </div>
                    <div class="card-body card-body-custom">
                    <h2 class="card-text"><?= $jumlahPegawai ?> Pegawai</h2>
                        <a href="<?= base_url('pegawai') ?>" class="btn btn-custom">Lihat Detail</a>
                    </div>
                </div>
                <!-- Another Card Example -->
                <div class="card card-custom">
                    <div class="card-header card-header-custom">
                        <h5 class="card-title card-title-custom">Manage User</h5>
                    </div>
                    <div class="card-body card-body-custom">
                        <h2 class="card-text"><?= $jumlahUser ?> User</h2>
                        <a href="<?= base_url('user') ?>" class="btn btn-custom">Lihat Detail</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
 
    <footer class="footer mt-auto py-3">
        <div class="container">
            <span class="text-muted">&copy; 2024 - E- Personal (Testing)</span>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://getbootstrap.com/docs/4.5/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
 
</html>