<?php 
$session = session();
$session->destroy();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
  
        .floating-card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            margin-left: 25px;
        }
        .floating-card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }
        .login-card {
            max-width: 500px;
            border: none;
        }
        .btn-login {
            background-color: rgb(10, 147, 158); 
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            box-shadow: 0 5px 7px 0 rgba(0,0,0,0.2);
        }
        .btn-login:hover {
            background-color: rgb(10, 147, 158);
            transition: 0.3s;
            box-shadow: 0 10px 16px 0 rgba(0,0,0,0.2);
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
        .form-floating>.form-control:focus~label,
        .form-floating>.form-control:not(:placeholder-shown)~label,
        .form-floating>.form-select~label {
            opacity: 1;
            transform: scale(.85) translateY(-.5rem) translateX(.15rem);
        }

        .form-floating {
            position: relative;
        }

        .form-floating>label {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            padding: 1rem .75rem;
            pointer-events: none;
            border: 1px solid transparent;
            transform-origin: 0 0;
            transition: opacity .1s ease-in-out, transform .1s ease-in-out;
            margin-bottom: 0;
        }
       
        @media (min-width: 992px) {
            .centered-form {
                max-width: 30%;
                margin: auto;
            }
        }

        @media (max-width: 991px) {
            .centered-form {
                max-width: 90%;
                margin: auto;
            }
        }
    
    </style>
    <title>Login - Page</title>
</head>
<body>
<div class="container-fluid p-0">
    <div class="row g-0 vh-100">
        <!-- Card for the image -->
        <div class="col-md-6 d-none d-md-flex align-items-center justify-content-center">
            <div class="floating-card">
                <img src="<?php echo base_url('images/side1.png'); ?>" class="img-fluid" alt="Responsive image">
            </div>
        </div>

        <!-- Card for the login form -->
        <div class="col-md-6 d-flex align-items-center justify-content-center">
            <div class="card login-card">
            <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
                <?php endif;?>
                <form action="<?= base_url('login/auth'); ?>" method="post" class="w-100 p-4">

                    <div class="card-header bg-white">
                        <h2>Masuk ke Aplikasi Elektronik Personal (E-Personal)</h2>
                        <p>Silahkan masukkan informasi akun anda.</p>
                    </div>
                    <div class="card-body">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" name="username" id="floatingUsername" placeholder="Username">
                            <label for="floatingUsername">Username</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="password" class="form-control" name="password" id="floatingPassword" placeholder="Password">
                            <label for="floatingPassword">Password</label>
                        </div>
                        <div class="d-grid">
                            <button type="submit" name="login" class="btn-login">Masuk</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
   
</body>
</html>
