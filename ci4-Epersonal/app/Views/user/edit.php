<?= $this->extend('layout/views-user'); ?>
<?= $this->section('content'); ?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Update Data User</h3>
        </div>
        <div class="card-body">
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <h4>Perhatikan isian Form!</h4>
                    </hr />
                    <?php echo session()->getFlashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <form method="post" action="<?= base_url('user/update/' . $user->id_user) ?>" enctype="multipart/form-data">
                <?= csrf_field(); ?>

                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $user->nama; ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="<?= $user->email; ?>" />
                </div>
                <div class="form-group">
                    <label for="email">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?= $user->username; ?>" />
                </div>

                <div class="form-group">
                    <label for="no_telp">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="<?= $user->password; ?>" />
                </div>

                <div class="form-group">
                    <label for="alamat">Photo</label>
                    <input type="file" class="form-control-file" id="photo" name="photo" value = "<?php $namaPhoto?>">
                    <label class="text-danger">*File foto maksimal 300kb</label><br>
                    <label class="text-danger">*Format foto jpg/jpeg</label>
                </div>

                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-info" />
                    <a href="<?= base_url('user'); ?>" class="btn btn-danger">Batal</a>
                </div>

            </form>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>