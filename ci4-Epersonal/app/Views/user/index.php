<?= $this->extend('layout/views-user'); ?>
<?= $this->section('content'); ?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Data User</h3>
        </div>
        <div class="card-body">
            <?php if (!empty(session()->getFlashdata('message'))) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('message'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <a href="<?= base_url('user/create'); ?>" class="btn btn-primary">Tambah</a>
            <hr />
            <div class="table-responsive">
            <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Foto</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $defaultPhoto = 'default.png'; 
                foreach ($user as $row) {
                    $displayPassword = strlen($row->password) > 20 ? substr($row->password, 0, 20) . '...' : $row->password;
                    // $photoPath = base_url('images/' . $row->photo);
                    $photoPath = FCPATH . 'images/' . $row->photo;
                    // $photoToShow = file_exists($photoPath) ? $photoPath : base_url('images/' . $defaultPhoto);
                    $photoToShow = is_file($photoPath) ? base_url('images/' . $row->photo) : base_url('images/' . $defaultPhoto);
                ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $row->nama; ?></td>
                        <td><?= $row->email; ?></td>
                        <td><?= $row->username; ?></td>
                        <td><?= $displayPassword; ?></td>
                        <td class="text-center"><img src="<?= $photoToShow ?>" alt="Foto" width="70"></td>
                        <!-- <td><img src='<?= $row->photo;'' ?></td> -->
                        <td>
                            <a title="Edit" href="<?= base_url("user/edit/$row->id_user"); ?>" class="btn btn-info">Edit</a>
                            <a title="Delete" href="<?= base_url("user/delete/$row->id_user") ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data <?= $row->nama ?> ?')">Delete</a>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
            </table>
            </div>
        </div>
        <div class="card-footer">
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                        <li class="page-item <?= $i == $currentPage ? 'active' : '' ?>">
                            <a class="page-link" href="<?= base_url('user/index/' . $i); ?>"><?= $i ?></a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>