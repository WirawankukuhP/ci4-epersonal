-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Jul 2024 pada 21.34
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pegawai`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin` enum('pria','wanita') NOT NULL,
  `no_telp` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `created_at` datetime(1) NOT NULL,
  `updated_at` datetime(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama`, `jenis_kelamin`, `no_telp`, `email`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'Syifa', 'wanita', '08129382931', 'syifa981@gmail.com', 'Jl. Sudaya No 181', '2024-07-05 01:39:47.0', '2024-07-05 03:09:53.0'),
(2, 'Komar', 'pria', '08762638293', 'komar76@gmail.com', 'Jl. Ampera No 220', '2024-07-05 01:39:47.0', '0000-00-00 00:00:00.0'),
(3, 'Jaka', 'pria', '08645267488', 'jakaminang@gmail.com', 'Jl. Lokapala No 87', '2024-07-05 01:39:47.0', '0000-00-00 00:00:00.0'),
(5, 'Ucup', 'pria', '0814455958646', 'ucup@yahoo.com', 'Jl. Gang Duren III', '2024-07-05 16:13:02.0', '2024-07-05 16:13:02.0'),
(6, 'Nurdin', 'pria', '0847468468464', 'nurdin7@yahoo.com', 'Jl. Kebangsaan 2', '2024-07-05 16:13:40.0', '2024-07-05 16:13:40.0'),
(7, 'Fatimah', 'wanita', '0811145454666', 'fatimah@yahoo.com', 'Jl. Muslim 4 ', '2024-07-05 16:14:24.0', '2024-07-05 16:14:24.0');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
